﻿namespace FactoryMethodTraining.Model
{
    public class DynamicFactory : MicrophoneFactory
    {
        public override Microphone CreateMicrophone()
        {
            return new SM57_Microphone();
        }
    }
}
