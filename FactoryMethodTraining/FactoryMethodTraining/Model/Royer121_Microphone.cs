﻿namespace FactoryMethodTraining.Model
{
    public class Royer121_Microphone : Microphone
    {
        string Microphone.DisplayInfo()
        {
            return "\nRibbon microphones are either the best thing ever or the worst, depending on who you ask. " +
                "They are, by some way, the most fragile, the most sensitive, and the most expensive. " +
                "But, you could say that about any vintage equipment, and it doesn’t make us want it any less.\n" +
                "These microphones were the standard in the 50s and 60s, two decades that produced some remarkable sounds. " +
                "Much of the current appeal of ribbon microphones is in trying to recreate some of that 50s/60s magic.\n";
        }
    }
}
