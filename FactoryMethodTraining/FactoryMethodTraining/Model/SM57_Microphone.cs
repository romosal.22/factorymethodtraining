﻿namespace FactoryMethodTraining.Model
{
    public class SM57_Microphone : Microphone
    {
        string Microphone.DisplayInfo()
        {
           return "\nDynamic microphones are the most robust and reliable microphones in the music industry. They are are" +
                " loved because they are built to endure a working life. Plus, if you go all out and break one, " +
                "they are the cheapest of the three microphone types to replace.\n";
        }
    }
}
