﻿namespace FactoryMethodTraining.Model
{
    public abstract class MicrophoneFactory
    {
        public abstract Microphone CreateMicrophone();
        public string Fabricate()
        {
            var microphone = CreateMicrophone();
            var info = microphone.DisplayInfo();
            return info;
        }
    }
}
