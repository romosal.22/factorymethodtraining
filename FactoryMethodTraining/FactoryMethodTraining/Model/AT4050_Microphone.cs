﻿namespace FactoryMethodTraining.Model
{
    public class AT4050_Microphone : Microphone
    {
        string Microphone.DisplayInfo()
        {
            return "\nCondenser microphones are a little more sophisticated than dynamic microphones." +
                " With that added sophistication comes added cost and fragility. In other words, they aren’t built" +
                " to take too many bumps, so be careful, or you could be out some serious cash.\n";
        }
    }
}
