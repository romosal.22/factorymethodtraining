﻿namespace FactoryMethodTraining.Model
{
    public interface Microphone
    {
        public string DisplayInfo();
    }
}
