﻿namespace FactoryMethodTraining.Model
{
    public class RibbonFactory : MicrophoneFactory
    {
        public override Microphone CreateMicrophone()
        {
            return new Royer121_Microphone();
        }
    }
}
