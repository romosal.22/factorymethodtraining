﻿namespace FactoryMethodTraining.Model
{
    public class CondenserFactory : MicrophoneFactory
    {
        public override Microphone CreateMicrophone()
        {
            return new AT4050_Microphone();
        }
    }
}
