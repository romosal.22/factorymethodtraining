﻿namespace FactoryMethodTraining.Model
{
    public class Client
    {
        
        public string Test()
        {
            string log = "";

            log = log + "App: Launched with the Dynamic Microphone Factory.\n";
            log = log + ClientCode(new DynamicFactory());

            log = log + ("App: Launched with the Condenser Microphone Factory.\n");
            log = log + ClientCode(new CondenserFactory());

            log = log + ("App: Launched with the Ribbon Microphone Factory.\n");
            log = log + ClientCode(new RibbonFactory());

            return log;
        }

        public string ClientCode(MicrophoneFactory creator)
        {

            return "Client: I'm not aware of the creator's class," +
                "but it still works.\n" + creator.Fabricate()+ "\n";

        }
    }
}
