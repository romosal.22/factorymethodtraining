using FactoryMethodTraining.Model;
using Microsoft.AspNetCore.Mvc;

namespace FactoryMethodTraining.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class MicrophoneFactoryController : ControllerBase
    {
      

        private readonly ILogger<MicrophoneFactoryController> _logger;

        public MicrophoneFactoryController(ILogger<MicrophoneFactoryController> logger)
        {
            _logger = logger;
        }

        [HttpGet(Name = "GetMicrophoneFactoryTest")]
        public String Get()
        {
            return new Client().Test();
        }
    }
}