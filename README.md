# FactoryMethodTraining

This Repository  holds  a simple .NET API that implements the Factory Method design pattern through a microphone factory example.

## Run the endpoint

run the next command

```
dotnet run
```
Then go to: http://localhost:{port}/swagger/index.html
The terminal is going to indicate in its logs the port number that the application is using.

## You can also open the project in Visual Studio 2022 and run it there.

